﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyBlog3.Models
{
    [Table("BaiViet")]
    public class Post
    {

        public int PostID { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(500, MinimumLength = 20, ErrorMessage = "Title phải có từ 20-500 ký tự")]

        public string Title { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(10000, MinimumLength = 50, ErrorMessage = "Body phải có ít nhất 50 ký tự")]
        public string Body { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày không hợp lệ")]
        public DateTime DateCreated { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày không hợp lệ")]
        public DateTime DateUpdated { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}