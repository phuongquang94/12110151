namespace MyBlog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Third : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TaiKhoan", "Password", c => c.String(nullable: false, maxLength: 18));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaiKhoan", "Password", c => c.String(nullable: false));
        }
    }
}
