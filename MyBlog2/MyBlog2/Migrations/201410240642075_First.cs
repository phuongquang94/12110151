namespace MyBlog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BaiViet",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 500),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        AccountID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PostID)
                .ForeignKey("dbo.TaiKhoan", t => t.AccountID, cascadeDelete: true)
                .Index(t => t.AccountID);
            
            CreateTable(
                "dbo.BinhLuan",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DayCreated = c.DateTime(nullable: false),
                        DayUpdated = c.DateTime(nullable: false),
                        Author = c.String(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BaiViet", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TaiKhoan",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.AccountID);
            
            CreateTable(
                "dbo.Tag_Post",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.BaiViet", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tag", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_Post", new[] { "TagID" });
            DropIndex("dbo.Tag_Post", new[] { "PostID" });
            DropIndex("dbo.BinhLuan", new[] { "PostID" });
            DropIndex("dbo.BaiViet", new[] { "AccountID" });
            DropForeignKey("dbo.Tag_Post", "TagID", "dbo.Tag");
            DropForeignKey("dbo.Tag_Post", "PostID", "dbo.BaiViet");
            DropForeignKey("dbo.BinhLuan", "PostID", "dbo.BaiViet");
            DropForeignKey("dbo.BaiViet", "AccountID", "dbo.TaiKhoan");
            DropTable("dbo.Tag_Post");
            DropTable("dbo.TaiKhoan");
            DropTable("dbo.Tag");
            DropTable("dbo.BinhLuan");
            DropTable("dbo.BaiViet");
        }
    }
}
