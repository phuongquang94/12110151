// <auto-generated />
namespace MyBlog2.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class Third : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Third));
        
        string IMigrationMetadata.Id
        {
            get { return "201410240700062_Third"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
