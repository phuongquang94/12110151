﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyBlog2.Models
{
     [Table("BaiViet")]
    public class Post
    {

        public int PostID { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(500, MinimumLength = 20, ErrorMessage = "Title phải có từ 20-500 ký tự")]

        public string Title { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(10000, MinimumLength = 50, ErrorMessage = "Body phải có ít nhất 50 ký tự")]
        public string Body { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày không hợp lệ")]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày không hợp lệ")]
        public DateTime DateUpdated { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        public int AccountID { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual Account Account { get; set; }
    }
}