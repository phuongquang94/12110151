﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyBlog2.Models
{
    public class MyBlog2DBContext: DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>()
                .HasMany(c => c.Tags)
                .WithMany(p => p.Posts)
                .Map(m => m.MapLeftKey("PostID")
                    .MapRightKey("TagID")
                    .ToTable("Tag_Post"));
        }
    }
}