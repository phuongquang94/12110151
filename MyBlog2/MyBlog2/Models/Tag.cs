﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyBlog2.Models
{
    [Table("Tag")]
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        [StringLength(100, ErrorMessage = "Số ký tự của của Content phải trong khoảng 10-100", MinimumLength = 10)]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}