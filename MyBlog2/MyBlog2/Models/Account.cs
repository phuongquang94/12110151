﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyBlog2.Models
{
    [Table("TaiKhoan")]
    public class Account
    {
        
        public int AccountID { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.Password)]
        [StringLength(18,ErrorMessage="Mật khẩu không được dưới 6 ký tự và trên 18 ký tự",MinimumLength=6)]
        public String Password { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        [EmailAddress(ErrorMessage = "Email bạn vừa nhập không hợp lệ, xin vui lòng nhập lại!")]
        public String Email { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        [StringLength(100, ErrorMessage = "Số ký tự của FirstName chỉ được tối đa 100 ký tự", MinimumLength = 1)]
        public String FirstName { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        [StringLength(100, ErrorMessage = "Số ký tự của LastName chỉ được tối đa 100 ký tự", MinimumLength = 1)]
        public String LastName { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}