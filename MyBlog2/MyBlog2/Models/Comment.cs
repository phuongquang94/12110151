﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyBlog2.Models
{
    [Table("BinhLuan")]
    public class Comment
    {
        
        public int ID { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        [StringLength(50000, ErrorMessage = "Số ký tự của của Body phải có tối thiểu 50 ký tự", MinimumLength = 50)]
        public String Body { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày không hợp lệ")]
        public DateTime DayCreated { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày không hợp lệ")]
        public DateTime DayUpdated { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        public String Author { set; get; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}