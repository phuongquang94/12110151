﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyBlog5.Models
{
    [Table("Tag")]
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống!")]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}