﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFinalProject.Models
{
    public class TheLoaiHinhAnh
    {
        public int TheLoaiHinhAnhID { set; get; }
        public String TenLoaiHinhAnh { set; get; }
        public virtual ICollection<HinhAnh> HinhAnhs { set; get; }
    }
}