﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFinalProject.Models
{
    public class Post
    {
        public int PostID { set; get; }
        public String TacGia { set; get; }
        public String TenBaiDang { set; get; }
        public DateTime NgayDang { set; get; }
        public String NoiDung { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Like> Likes { set; get; }
        public virtual ICollection<HinhAnh> HinhAnh { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserId { set; get; }
        public virtual ChiTietTheLoaiTruyen ChiTietTheLoaiTruyen { set; get; }
        public int ChiTietTheLoaiTruyenID { set; get; }
    }
}