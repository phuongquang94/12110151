﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFinalProject.Models
{
    public class HinhAnh
    {
        public int HinhAnhID { set; get; }
        public String TenHinhAnh { set; get; }
        public String HinhAnhUrl { set; get; }
        public virtual Post Post { set; get; }
        public int PostID { set; get; }
        public virtual TheLoaiHinhAnh TheLoaiHinhAnh { set; get; }
        public int TheLoaiHinhAnhID { set; get; }
    }
}