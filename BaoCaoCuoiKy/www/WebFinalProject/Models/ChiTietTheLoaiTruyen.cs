﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFinalProject.Models
{
    public class ChiTietTheLoaiTruyen
    {
        public int ChiTietTheLoaiTruyenID { set; get; }
        public String TenChiTietTheLoaiTruyen { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
        public virtual LoaiTruyen LoaiTruyen { set; get; }
        public int LoaiTruyenID { set; get; }
    }
}