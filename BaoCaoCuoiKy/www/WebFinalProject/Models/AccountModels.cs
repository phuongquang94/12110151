﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace WebFinalProject.Models
{
    public class WebProjectContext : DbContext
    {
        public WebProjectContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<ChiTietTheLoaiTruyen> ChiTietTheLoaiTruyen { set; get; }
        public DbSet<Comment> Comment { set; get; }
        public DbSet<HinhAnh> HinhAnh { set; get; }
        public DbSet<TheLoaiHinhAnh> TheLoaiHinhAnh { set; get; }
        public DbSet<Like> Like { set; get; }
        public DbSet<LoaiTruyen> LoaiTruyen { set; get; }
        public DbSet<TuocVi> TuocVi { set; get; }
        public DbSet<Tag> Tag { set; get; }
        public DbSet<Post> Post { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>()
                .HasMany(t => t.Tags)
                .WithMany(b => b.Posts)
                .Map(m => m.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Posts_Tags"));
            //modelBuilder.Entity<UserProfile>()
            //    .HasOptional(p => p.Profile)
            //    .WithRequired(a => a.UserProfile);
        }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public virtual ICollection<Post> Posts { set; get; }
        public String TenNguoiDung { set; get; }
        public String HinhDaiDien { set; get; }
        public DateTime NgaySinh { set; get; }
        public String GioiTinh { set; get; }
        public String DiaChi { set; get; }
        public String Email { set; get; }
        public String SoDienThoai { set; get; }
        public String SoThich { set; get; }
        public virtual TuocVi TuocVi { set; get; }
        public int TuocViID { set; get; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public String TenNguoiDung { set; get; }
        public DateTime NgaySinh { set; get; }
        public String GioiTinh { set; get; }
        public String DiaChi { set; get; }
        public String Email { set; get; }
        public String SoDienThoai { set; get; }
        public String SoThich { set; get; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
