﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFinalProject.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        public String NoiDung { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}