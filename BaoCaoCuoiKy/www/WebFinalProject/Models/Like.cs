﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFinalProject.Models
{
    public class Like
    {
        public int LikeID { set; get; }
        public int UserProfileUserId { set; get; }
        public DateTime NgayThich { set; get; }
        public virtual Post Post { set; get; }
        public int PostID { set; get; }
    }
}