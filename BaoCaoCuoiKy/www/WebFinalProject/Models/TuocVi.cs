﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFinalProject.Models
{
    public class TuocVi
    {
        public int TuocViID { set; get; }
        public String TenTuocVi { set; get; }
        public String DieuKienBaiDang { set; get; }
        public virtual ICollection<UserProfile> UserProfile { set; get; }
    }
}