﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFinalProject.Models
{
    public class LoaiTruyen
    {
        public int LoaiTruyenID { set; get; }
        public String TenLoaiTruyen { set; get; }
        public virtual ICollection<ChiTietTheLoaiTruyen> ChiTietTheLoaiTruyens { set; get; }
    }
}