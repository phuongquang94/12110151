﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFinalProject.Models
{
    public class Comment
    {
        public int CommentID { set; get; }
        public int UserProfileUserId { set; get; }
        public String NoiDung { set; get; }
        public DateTime NgayBinhLuan { set; get; }
        public virtual Post Post { set; get; }
        public int PostID { set; get; }
        public String LastTime
        {
            get
            {
                if (Int32.Parse((DateTime.Now - NgayBinhLuan).Minutes.ToString()) >= 1 && Int32.Parse((DateTime.Now - NgayBinhLuan).Hours.ToString()) < 1)
                {
                    return (DateTime.Now - NgayBinhLuan).Minutes.ToString() + " phút";
                }
                else if (Int32.Parse((DateTime.Now - NgayBinhLuan).Hours.ToString()) >= 1 && Int32.Parse((DateTime.Now - NgayBinhLuan).Days.ToString())<1)
                {
                    return (DateTime.Now - NgayBinhLuan).Hours.ToString() + " giờ";
                }
                else if (Int32.Parse((DateTime.Now - NgayBinhLuan).Days.ToString()) >=1 )
                {
                    return (DateTime.Now - NgayBinhLuan).Days.ToString() + " ngày";
                }
                else
                {
                    return (DateTime.Now - NgayBinhLuan).Seconds.ToString() + " giây";
                }
            }
        }
    }
}