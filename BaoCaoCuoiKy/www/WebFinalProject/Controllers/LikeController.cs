﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFinalProject.Models;

namespace WebFinalProject.Controllers
{
    public class LikeController : Controller
    {
        private WebProjectContext db = new WebProjectContext();

        //
        // GET: /Like/

        public ActionResult Index()
        {
            var like = db.Like.Include(l => l.Post);
            return View(like.ToList());
        }

        //
        // GET: /Like/Details/5

        public ActionResult Details(int id = 0)
        {
            Like like = db.Like.Find(id);
            if (like == null)
            {
                return HttpNotFound();
            }
            return View(like);
        }

        //
        // GET: /Like/Create

        public ActionResult Create()
        {
            ViewBag.PostID = new SelectList(db.Post, "PostID", "TacGia");
            return View();
        }

        //
        // POST: /Like/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Like like)
        {
            if (ModelState.IsValid)
            {
                db.Like.Add(like);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostID = new SelectList(db.Post, "PostID", "TacGia", like.PostID);
            return View(like);
        }

        //
        // GET: /Like/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Like like = db.Like.Find(id);
            if (like == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostID = new SelectList(db.Post, "PostID", "TacGia", like.PostID);
            return View(like);
        }

        //
        // POST: /Like/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Like like)
        {
            if (ModelState.IsValid)
            {
                db.Entry(like).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostID = new SelectList(db.Post, "PostID", "TacGia", like.PostID);
            return View(like);
        }

        //
        // GET: /Like/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Like like = db.Like.Find(id);
            if (like == null)
            {
                return HttpNotFound();
            }
            return View(like);
        }

        //
        // POST: /Like/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Like like = db.Like.Find(id);
            db.Like.Remove(like);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}