﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFinalProject.Models;

namespace WebFinalProject.Controllers
{
    public class HinhAnhController : Controller
    {
        private WebProjectContext db = new WebProjectContext();

        //
        // GET: /HinhAnh/

        public ActionResult Index()
        {
            var hinhanh = db.HinhAnh.Include(h => h.Post).Include(h => h.TheLoaiHinhAnh);
            return View(hinhanh.ToList());
        }

        //
        // GET: /HinhAnh/Details/5

        public ActionResult Details(int id = 0)
        {
            HinhAnh hinhanh = db.HinhAnh.Find(id);
            if (hinhanh == null)
            {
                return HttpNotFound();
            }
            return View(hinhanh);
        }

        //
        // GET: /HinhAnh/Create

        public ActionResult Create()
        {
            ViewBag.PostID = new SelectList(db.Post, "PostID", "TacGia");
            ViewBag.TheLoaiHinhAnhID = new SelectList(db.TheLoaiHinhAnh, "TheLoaiHinhAnhID", "TenLoaiHinhAnh");
            return View();
        }

        //
        // POST: /HinhAnh/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HinhAnh hinhanh)
        {
            if (ModelState.IsValid)
            {
                db.HinhAnh.Add(hinhanh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostID = new SelectList(db.Post, "PostID", "TacGia", hinhanh.PostID);
            ViewBag.TheLoaiHinhAnhID = new SelectList(db.TheLoaiHinhAnh, "TheLoaiHinhAnhID", "TenLoaiHinhAnh", hinhanh.TheLoaiHinhAnhID);
            return View(hinhanh);
        }

        //
        // GET: /HinhAnh/Edit/5

        public ActionResult Edit(int id = 0)
        {
            HinhAnh hinhanh = db.HinhAnh.Find(id);
            if (hinhanh == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostID = new SelectList(db.Post, "PostID", "TacGia", hinhanh.PostID);
            ViewBag.TheLoaiHinhAnhID = new SelectList(db.TheLoaiHinhAnh, "TheLoaiHinhAnhID", "TenLoaiHinhAnh", hinhanh.TheLoaiHinhAnhID);
            return View(hinhanh);
        }

        //
        // POST: /HinhAnh/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HinhAnh hinhanh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hinhanh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostID = new SelectList(db.Post, "PostID", "TacGia", hinhanh.PostID);
            ViewBag.TheLoaiHinhAnhID = new SelectList(db.TheLoaiHinhAnh, "TheLoaiHinhAnhID", "TenLoaiHinhAnh", hinhanh.TheLoaiHinhAnhID);
            return View(hinhanh);
        }

        //
        // GET: /HinhAnh/Delete/5

        public ActionResult Delete(int id = 0)
        {
            HinhAnh hinhanh = db.HinhAnh.Find(id);
            if (hinhanh == null)
            {
                return HttpNotFound();
            }
            return View(hinhanh);
        }

        //
        // POST: /HinhAnh/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HinhAnh hinhanh = db.HinhAnh.Find(id);
            db.HinhAnh.Remove(hinhanh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}