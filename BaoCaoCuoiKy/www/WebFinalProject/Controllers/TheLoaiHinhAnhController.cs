﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFinalProject.Models;

namespace WebFinalProject.Controllers
{
    public class TheLoaiHinhAnhController : Controller
    {
        private WebProjectContext db = new WebProjectContext();

        //
        // GET: /TheLoaiHinhAnh/

        public ActionResult Index()
        {
            return View(db.TheLoaiHinhAnh.ToList());
        }

        //
        // GET: /TheLoaiHinhAnh/Details/5

        public ActionResult Details(int id = 0)
        {
            TheLoaiHinhAnh theloaihinhanh = db.TheLoaiHinhAnh.Find(id);
            if (theloaihinhanh == null)
            {
                return HttpNotFound();
            }
            return View(theloaihinhanh);
        }

        //
        // GET: /TheLoaiHinhAnh/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TheLoaiHinhAnh/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TheLoaiHinhAnh theloaihinhanh)
        {
            if (ModelState.IsValid)
            {
                db.TheLoaiHinhAnh.Add(theloaihinhanh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(theloaihinhanh);
        }

        //
        // GET: /TheLoaiHinhAnh/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TheLoaiHinhAnh theloaihinhanh = db.TheLoaiHinhAnh.Find(id);
            if (theloaihinhanh == null)
            {
                return HttpNotFound();
            }
            return View(theloaihinhanh);
        }

        //
        // POST: /TheLoaiHinhAnh/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TheLoaiHinhAnh theloaihinhanh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(theloaihinhanh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(theloaihinhanh);
        }

        //
        // GET: /TheLoaiHinhAnh/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TheLoaiHinhAnh theloaihinhanh = db.TheLoaiHinhAnh.Find(id);
            if (theloaihinhanh == null)
            {
                return HttpNotFound();
            }
            return View(theloaihinhanh);
        }

        //
        // POST: /TheLoaiHinhAnh/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TheLoaiHinhAnh theloaihinhanh = db.TheLoaiHinhAnh.Find(id);
            db.TheLoaiHinhAnh.Remove(theloaihinhanh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}