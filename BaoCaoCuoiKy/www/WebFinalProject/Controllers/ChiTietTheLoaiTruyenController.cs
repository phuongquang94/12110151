﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFinalProject.Models;

namespace WebFinalProject.Controllers
{
    public class ChiTietTheLoaiTruyenController : Controller
    {
        private WebProjectContext db = new WebProjectContext();

        //
        // GET: /ChiTietTheLoaiTruyen/

        public ActionResult Index()
        {
            var chitiettheloaitruyen = db.ChiTietTheLoaiTruyen.Include(c => c.LoaiTruyen);
            return View(chitiettheloaitruyen.ToList());
        }

        //
        // GET: /ChiTietTheLoaiTruyen/Details/5

        public ActionResult Details(int id = 0)
        {
            ChiTietTheLoaiTruyen chitiettheloaitruyen = db.ChiTietTheLoaiTruyen.Find(id);
            if (chitiettheloaitruyen == null)
            {
                return HttpNotFound();
            }
            return View(chitiettheloaitruyen);
        }

        //
        // GET: /ChiTietTheLoaiTruyen/Create

        public ActionResult Create()
        {
            ViewBag.LoaiTruyenID = new SelectList(db.LoaiTruyen, "LoaiTruyenID", "TenLoaiTruyen");
            return View();
        }

        //
        // POST: /ChiTietTheLoaiTruyen/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ChiTietTheLoaiTruyen chitiettheloaitruyen)
        {
            if (ModelState.IsValid)
            {
                db.ChiTietTheLoaiTruyen.Add(chitiettheloaitruyen);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LoaiTruyenID = new SelectList(db.LoaiTruyen, "LoaiTruyenID", "TenLoaiTruyen", chitiettheloaitruyen.LoaiTruyenID);
            return View(chitiettheloaitruyen);
        }

        //
        // GET: /ChiTietTheLoaiTruyen/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ChiTietTheLoaiTruyen chitiettheloaitruyen = db.ChiTietTheLoaiTruyen.Find(id);
            if (chitiettheloaitruyen == null)
            {
                return HttpNotFound();
            }
            ViewBag.LoaiTruyenID = new SelectList(db.LoaiTruyen, "LoaiTruyenID", "TenLoaiTruyen", chitiettheloaitruyen.LoaiTruyenID);
            return View(chitiettheloaitruyen);
        }

        //
        // POST: /ChiTietTheLoaiTruyen/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ChiTietTheLoaiTruyen chitiettheloaitruyen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chitiettheloaitruyen).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LoaiTruyenID = new SelectList(db.LoaiTruyen, "LoaiTruyenID", "TenLoaiTruyen", chitiettheloaitruyen.LoaiTruyenID);
            return View(chitiettheloaitruyen);
        }

        //
        // GET: /ChiTietTheLoaiTruyen/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ChiTietTheLoaiTruyen chitiettheloaitruyen = db.ChiTietTheLoaiTruyen.Find(id);
            if (chitiettheloaitruyen == null)
            {
                return HttpNotFound();
            }
            return View(chitiettheloaitruyen);
        }

        //
        // POST: /ChiTietTheLoaiTruyen/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChiTietTheLoaiTruyen chitiettheloaitruyen = db.ChiTietTheLoaiTruyen.Find(id);
            db.ChiTietTheLoaiTruyen.Remove(chitiettheloaitruyen);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}