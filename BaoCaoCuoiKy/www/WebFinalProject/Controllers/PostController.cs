﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebFinalProject.Models;

namespace WebFinalProject.Controllers
{
    public class PostController : Controller
    {
        private WebProjectContext db = new WebProjectContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            var post = db.Post.Include(p => p.ChiTietTheLoaiTruyen);
            return View(post.ToList());
        }
        public ActionResult AnhDuongLaCuaToi(int id=1)
        
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }
        public ActionResult SearchTags(int id = 0)
        {
            Tag tag = db.Tag.Find(id);
            return View(tag);
        }
        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 1)
        {

            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }
         public ActionResult ResultAfterCreatePost()
        {
            return View();
        }
        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            ViewBag.ChiTietTheLoaiTruyenID = new SelectList(db.ChiTietTheLoaiTruyen, "ChiTietTheLoaiTruyenID", "TenChiTietTheLoaiTruyen");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, string NoiDung)
        {
            if (ModelState.IsValid)
            {
                post.NgayDang = DateTime.Now;
                post.UserId = db.UserProfiles.Where(p => p.UserName == User.Identity.Name).Select(a => a.UserId).Single();
                List<Tag> Tags = new List<Tag>();
                string[] TagContent = NoiDung.Split(',');
                foreach (string item in TagContent)
                {
                    Tag tagExits = null;
                    var ListTag = db.Tag.Where(y => y.NoiDung.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        tagExits = ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        tagExits = new Tag();
                        tagExits.NoiDung = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }
                    Tags.Add(tagExits);
                }
                post.Tags = Tags;

                db.Post.Add(post);
                db.SaveChanges();
                return RedirectToAction("ResultAfterCreatePost");
            }

            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserId);
            ViewBag.ChiTietTheLoaiTruyenID = new SelectList(db.ChiTietTheLoaiTruyen, "ChiTietTheLoaiTruyenID", "TenChiTietTheLoaiTruyen", post.ChiTietTheLoaiTruyenID);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserId);
            ViewBag.ChiTietTheLoaiTruyenID = new SelectList(db.ChiTietTheLoaiTruyen, "ChiTietTheLoaiTruyenID", "TenChiTietTheLoaiTruyen", post.ChiTietTheLoaiTruyenID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserId);
            ViewBag.ChiTietTheLoaiTruyenID = new SelectList(db.ChiTietTheLoaiTruyen, "ChiTietTheLoaiTruyenID", "TenChiTietTheLoaiTruyen", post.ChiTietTheLoaiTruyenID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Post.Find(id);
            db.Post.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}