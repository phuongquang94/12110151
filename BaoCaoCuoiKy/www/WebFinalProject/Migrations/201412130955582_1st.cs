namespace WebFinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1st : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        TuocVi_TuocViID = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.TuocVis", t => t.TuocVi_TuocViID)
                .Index(t => t.TuocVi_TuocViID);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        TacGia = c.String(),
                        TenBaiDang = c.String(),
                        NgayDang = c.DateTime(nullable: false),
                        NoiDung = c.String(),
                        UserId = c.Int(nullable: false),
                        ChiTietTheLoaiTruyenID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PostID)
                .ForeignKey("dbo.UserProfile", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.ChiTietTheLoaiTruyens", t => t.ChiTietTheLoaiTruyenID, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ChiTietTheLoaiTruyenID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        UserProfileUserId = c.Int(nullable: false),
                        NoiDung = c.String(),
                        NgayBinhLuan = c.DateTime(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        LikeID = c.Int(nullable: false, identity: true),
                        UserProfileUserId = c.Int(nullable: false),
                        NgayThich = c.DateTime(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LikeID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.HinhAnhs",
                c => new
                    {
                        HinhAnhID = c.Int(nullable: false, identity: true),
                        TenHinhAnh = c.String(),
                        HinhAnhUrl = c.String(),
                        PostID = c.Int(nullable: false),
                        TheLoaiHinhAnhID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HinhAnhID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.TheLoaiHinhAnhs", t => t.TheLoaiHinhAnhID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TheLoaiHinhAnhID);
            
            CreateTable(
                "dbo.TheLoaiHinhAnhs",
                c => new
                    {
                        TheLoaiHinhAnhID = c.Int(nullable: false, identity: true),
                        TenLoaiHinhAnh = c.String(),
                    })
                .PrimaryKey(t => t.TheLoaiHinhAnhID);
            
            CreateTable(
                "dbo.ChiTietTheLoaiTruyens",
                c => new
                    {
                        ChiTietTheLoaiTruyenID = c.Int(nullable: false, identity: true),
                        TenChiTietTheLoaiTruyen = c.String(),
                        LoaiTruyenID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ChiTietTheLoaiTruyenID)
                .ForeignKey("dbo.LoaiTruyens", t => t.LoaiTruyenID, cascadeDelete: true)
                .Index(t => t.LoaiTruyenID);
            
            CreateTable(
                "dbo.LoaiTruyens",
                c => new
                    {
                        LoaiTruyenID = c.Int(nullable: false, identity: true),
                        TenLoaiTruyen = c.String(),
                    })
                .PrimaryKey(t => t.LoaiTruyenID);
            
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        UserProfileUserId = c.Int(nullable: false),
                        TenNguoiDung = c.String(),
                        HinhDaiDien = c.String(),
                        NgaySinh = c.DateTime(nullable: false),
                        GioiTinh = c.String(),
                        DiaChi = c.String(),
                        Email = c.String(),
                        SoDienThoai = c.String(),
                        SoThich = c.String(),
                        TuocViID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserProfileUserId)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId)
                .ForeignKey("dbo.TuocVis", t => t.TuocViID, cascadeDelete: true)
                .Index(t => t.UserProfileUserId)
                .Index(t => t.TuocViID);
            
            CreateTable(
                "dbo.TuocVis",
                c => new
                    {
                        TuocViID = c.Int(nullable: false, identity: true),
                        TenTuocVi = c.String(),
                        DieuKienBaiDang = c.String(),
                    })
                .PrimaryKey(t => t.TuocViID);
            
            CreateTable(
                "dbo.Posts_Tags",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts_Tags", new[] { "TagID" });
            DropIndex("dbo.Posts_Tags", new[] { "PostID" });
            DropIndex("dbo.Profiles", new[] { "TuocViID" });
            DropIndex("dbo.Profiles", new[] { "UserProfileUserId" });
            DropIndex("dbo.ChiTietTheLoaiTruyens", new[] { "LoaiTruyenID" });
            DropIndex("dbo.HinhAnhs", new[] { "TheLoaiHinhAnhID" });
            DropIndex("dbo.HinhAnhs", new[] { "PostID" });
            DropIndex("dbo.Likes", new[] { "PostID" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "ChiTietTheLoaiTruyenID" });
            DropIndex("dbo.Posts", new[] { "UserId" });
            DropIndex("dbo.UserProfile", new[] { "TuocVi_TuocViID" });
            DropForeignKey("dbo.Posts_Tags", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Posts_Tags", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Profiles", "TuocViID", "dbo.TuocVis");
            DropForeignKey("dbo.Profiles", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.ChiTietTheLoaiTruyens", "LoaiTruyenID", "dbo.LoaiTruyens");
            DropForeignKey("dbo.HinhAnhs", "TheLoaiHinhAnhID", "dbo.TheLoaiHinhAnhs");
            DropForeignKey("dbo.HinhAnhs", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Likes", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "ChiTietTheLoaiTruyenID", "dbo.ChiTietTheLoaiTruyens");
            DropForeignKey("dbo.Posts", "UserId", "dbo.UserProfile");
            DropForeignKey("dbo.UserProfile", "TuocVi_TuocViID", "dbo.TuocVis");
            DropTable("dbo.Posts_Tags");
            DropTable("dbo.TuocVis");
            DropTable("dbo.Profiles");
            DropTable("dbo.LoaiTruyens");
            DropTable("dbo.ChiTietTheLoaiTruyens");
            DropTable("dbo.TheLoaiHinhAnhs");
            DropTable("dbo.HinhAnhs");
            DropTable("dbo.Likes");
            DropTable("dbo.Comments");
            DropTable("dbo.Tags");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfile");
        }
    }
}
