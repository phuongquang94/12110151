namespace WebFinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3rd : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Profiles", "UserProfileUserId", "dbo.UserProfile");
            DropIndex("dbo.Profiles", new[] { "UserProfileUserId" });
            DropTable("dbo.Profiles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserProfileUserId);
            
            CreateIndex("dbo.Profiles", "UserProfileUserId");
            AddForeignKey("dbo.Profiles", "UserProfileUserId", "dbo.UserProfile", "UserId");
        }
    }
}
