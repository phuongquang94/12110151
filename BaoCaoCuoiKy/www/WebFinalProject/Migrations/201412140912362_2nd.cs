namespace WebFinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2nd : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserProfile", "TuocVi_TuocViID", "dbo.TuocVis");
            DropForeignKey("dbo.Profiles", "TuocViID", "dbo.TuocVis");
            DropIndex("dbo.UserProfile", new[] { "TuocVi_TuocViID" });
            DropIndex("dbo.Profiles", new[] { "TuocViID" });
            RenameColumn(table: "dbo.UserProfile", name: "TuocVi_TuocViID", newName: "TuocViID");
            AddColumn("dbo.UserProfile", "TenNguoiDung", c => c.String());
            AddColumn("dbo.UserProfile", "HinhDaiDien", c => c.String());
            AddColumn("dbo.UserProfile", "NgaySinh", c => c.DateTime(nullable: false));
            AddColumn("dbo.UserProfile", "GioiTinh", c => c.String());
            AddColumn("dbo.UserProfile", "DiaChi", c => c.String());
            AddColumn("dbo.UserProfile", "Email", c => c.String());
            AddColumn("dbo.UserProfile", "SoDienThoai", c => c.String());
            AddColumn("dbo.UserProfile", "SoThich", c => c.String());
            AddForeignKey("dbo.UserProfile", "TuocViID", "dbo.TuocVis", "TuocViID", cascadeDelete: true);
            CreateIndex("dbo.UserProfile", "TuocViID");
            DropColumn("dbo.Profiles", "TenNguoiDung");
            DropColumn("dbo.Profiles", "HinhDaiDien");
            DropColumn("dbo.Profiles", "NgaySinh");
            DropColumn("dbo.Profiles", "GioiTinh");
            DropColumn("dbo.Profiles", "DiaChi");
            DropColumn("dbo.Profiles", "Email");
            DropColumn("dbo.Profiles", "SoDienThoai");
            DropColumn("dbo.Profiles", "SoThich");
            DropColumn("dbo.Profiles", "TuocViID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Profiles", "TuocViID", c => c.Int(nullable: false));
            AddColumn("dbo.Profiles", "SoThich", c => c.String());
            AddColumn("dbo.Profiles", "SoDienThoai", c => c.String());
            AddColumn("dbo.Profiles", "Email", c => c.String());
            AddColumn("dbo.Profiles", "DiaChi", c => c.String());
            AddColumn("dbo.Profiles", "GioiTinh", c => c.String());
            AddColumn("dbo.Profiles", "NgaySinh", c => c.DateTime(nullable: false));
            AddColumn("dbo.Profiles", "HinhDaiDien", c => c.String());
            AddColumn("dbo.Profiles", "TenNguoiDung", c => c.String());
            DropIndex("dbo.UserProfile", new[] { "TuocViID" });
            DropForeignKey("dbo.UserProfile", "TuocViID", "dbo.TuocVis");
            DropColumn("dbo.UserProfile", "SoThich");
            DropColumn("dbo.UserProfile", "SoDienThoai");
            DropColumn("dbo.UserProfile", "Email");
            DropColumn("dbo.UserProfile", "DiaChi");
            DropColumn("dbo.UserProfile", "GioiTinh");
            DropColumn("dbo.UserProfile", "NgaySinh");
            DropColumn("dbo.UserProfile", "HinhDaiDien");
            DropColumn("dbo.UserProfile", "TenNguoiDung");
            RenameColumn(table: "dbo.UserProfile", name: "TuocViID", newName: "TuocVi_TuocViID");
            CreateIndex("dbo.Profiles", "TuocViID");
            CreateIndex("dbo.UserProfile", "TuocVi_TuocViID");
            AddForeignKey("dbo.Profiles", "TuocViID", "dbo.TuocVis", "TuocViID", cascadeDelete: true);
            AddForeignKey("dbo.UserProfile", "TuocVi_TuocViID", "dbo.TuocVis", "TuocViID");
        }
    }
}
