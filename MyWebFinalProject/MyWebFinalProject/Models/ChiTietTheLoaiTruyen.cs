﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("ChiTietTheLoaiTruyen")]
    public class ChiTietTheLoaiTruyen
    {
        public int ChiTietTheLoaiTruyenID { set; get; }
        public String TenChiTietTheLoaiTruyen { set; get; }
        public virtual ICollection<BaiDang> BaiDangs { set; get; }
        public virtual LoaiTruyen LoaiTruyen { set; get; }
        public int LoaiTruyenID { set; get; }
    }
}