﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("BinhLuan")]
    public class Comment
    {
        public int CommentID { set; get; }
        public int AccountID { set; get; }
        public String NoiDung { set; get; }
        public DateTime NgayComment { set; get; }
        public virtual BaiDang BaiDang { set; get; }
        public int BaiDangID { set; get; }
    }
}