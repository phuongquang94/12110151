﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("TuocVi")]
    public class TuocVi
    {
        public int TuocViID { set; get; }
        public String TenTuocVi { set; get; }
        public String DieuKienBaiDang { set; get; }
        public virtual ICollection<Account> Accounts { set; get; }
    }
}