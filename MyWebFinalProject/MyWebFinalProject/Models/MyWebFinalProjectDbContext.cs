﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    public class MyWebFinalProjectDbContext: DbContext
    {
        public DbSet<Account> Accounts { set; get; }
        public DbSet<BaiDang> BaiDangs { set; get; }
        public DbSet<ChiTietTheLoaiTruyen> ChiTietTheLoaiTruyens { set; get; }
        public DbSet<Comment> Comments { set; get; }
        public DbSet<HinhAnh> HinhAnhs { set; get; }
        public DbSet<Like> Likes { set; get; }
        public DbSet<LoaiTruyen> LoaiTruyens { set; get; }
        public DbSet<Tag> Tags { set; get; }
        public DbSet<TheLoaiHinhAnh> TheLoaiHinhAnhs { set; get; }
        public DbSet<TuocVi> TuocVis { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<BaiDang>()
                .HasMany(t => t.Tags)
                .WithMany(b => b.BaiDangs)
                .Map(m => m.MapLeftKey("BaiDangID").MapRightKey("TagID").ToTable("BaiDangs_Tags"));
        }
        
    }
}