﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("TheLoaiHinhAnh")]
    public class TheLoaiHinhAnh
    {
        public int TheLoaiHinhAnhID { set; get; }
        public String TenLoaiHinhAnh { set; get; }
        public virtual ICollection<HinhAnh> HinhAnhs { set; get; }
    }
}