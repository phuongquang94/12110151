﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("Thich")]
    public class Like
    {
        public int LikeID { set; get; }
        public int AccountID { set; get; }
        public String NgayThich { set; get; }
        public virtual BaiDang BaiDang { set; get; }
        public int BaiDangID { set; get; }
    }
}