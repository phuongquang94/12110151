﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("Tag")]
    public class Tag
    {
        public int TagID { set; get; }
        public String NoiDung { set; get; }
        public virtual ICollection<BaiDang> BaiDangs { set; get; }
    }
}