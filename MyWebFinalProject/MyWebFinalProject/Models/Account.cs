﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("TaiKhoan")]
    public class Account
    {
        public int AccountID { set; get; }
        public String TenTaiKhoan { set; get; }
        public String TenNguoiDung { set; get; }
        public String TenHienThi { set; get; }
        public DateTime NgaySinh { set; get; }
        public String GioiTinh { set; get; }
        public String DiaChi { set; get; }
        public String Email { set; get; }
        public String SoDienThoai { set; get; }
        public String SoThich { set; get; }
        public virtual TuocVi TuocVi { set; get; }
        public int TuocViID { set; get; }
        public virtual ICollection<BaiDang> BaiDangs { set; get; }
    }
}