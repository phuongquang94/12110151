﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("LoaiTruyen")]
    public class LoaiTruyen
    {
        public int LoaiTruyenID { set; get; }
        public String TenLoaiTruyen { set; get; }
        public virtual ICollection<ChiTietTheLoaiTruyen> ChiTietTheLoaiTruyens { set; get; }
    }
}