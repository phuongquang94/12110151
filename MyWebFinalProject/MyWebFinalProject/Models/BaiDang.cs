﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("BaiDang")]
    public class BaiDang
    {
        public int BaiDangID { set; get; }
        public String TacGia { set; get; }
        public String TenBaiDang { set; get; }
        public DateTime NgayDang { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Like> Likes { set; get; }
        public virtual ICollection<HinhAnh> HinhAnh { set; get; }
        public virtual Account Account { set; get; }
        public int AccountID { set; get; }
        public virtual ChiTietTheLoaiTruyen ChiTietTheLoaiTruyen { set; get; }
        public int ChiTietTheLoaiTruyenID { set; get; }

    }
}