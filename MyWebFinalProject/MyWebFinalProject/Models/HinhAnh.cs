﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebFinalProject.Models
{
    [Table("HinhAnh")]
    public class HinhAnh
    {
        public int HinhAnhID {set; get;}
        public String TenHinhAnh { set; get; }
        public virtual BaiDang BaiDang { set; get; }
        public int BaiDangID { set; get; }
        public virtual TheLoaiHinhAnh TheLoaiHinhAnh { set; get; }
        public int TheLoaiHinhAnhID { set; get; }

    }
}