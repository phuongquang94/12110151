namespace MyWebFinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        TuocViID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountID)
                .ForeignKey("dbo.TuocVis", t => t.TuocViID, cascadeDelete: true)
                .Index(t => t.TuocViID);
            
            CreateTable(
                "dbo.TuocVis",
                c => new
                    {
                        TuocViID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.TuocViID);
            
            CreateTable(
                "dbo.BaiDangs",
                c => new
                    {
                        BaiDangID = c.Int(nullable: false, identity: true),
                        AccountID = c.Int(nullable: false),
                        ChiTietTheLoaiTruyenID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BaiDangID)
                .ForeignKey("dbo.Accounts", t => t.AccountID, cascadeDelete: true)
                .ForeignKey("dbo.ChiTietTheLoaiTruyens", t => t.ChiTietTheLoaiTruyenID, cascadeDelete: true)
                .Index(t => t.AccountID)
                .Index(t => t.ChiTietTheLoaiTruyenID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        BaiDangID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.BaiDangs", t => t.BaiDangID, cascadeDelete: true)
                .Index(t => t.BaiDangID);
            
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        LikeID = c.Int(nullable: false, identity: true),
                        BaiDangID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LikeID)
                .ForeignKey("dbo.BaiDangs", t => t.BaiDangID, cascadeDelete: true)
                .Index(t => t.BaiDangID);
            
            CreateTable(
                "dbo.HinhAnhs",
                c => new
                    {
                        HinhAnhID = c.Int(nullable: false, identity: true),
                        BaiDangID = c.Int(nullable: false),
                        TheLoaiHinhAnhID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HinhAnhID)
                .ForeignKey("dbo.BaiDangs", t => t.BaiDangID, cascadeDelete: true)
                .ForeignKey("dbo.TheLoaiHinhAnhs", t => t.TheLoaiHinhAnhID, cascadeDelete: true)
                .Index(t => t.BaiDangID)
                .Index(t => t.TheLoaiHinhAnhID);
            
            CreateTable(
                "dbo.TheLoaiHinhAnhs",
                c => new
                    {
                        TheLoaiHinhAnhID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.TheLoaiHinhAnhID);
            
            CreateTable(
                "dbo.ChiTietTheLoaiTruyens",
                c => new
                    {
                        ChiTietTheLoaiTruyenID = c.Int(nullable: false, identity: true),
                        LoaiTruyenID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ChiTietTheLoaiTruyenID)
                .ForeignKey("dbo.LoaiTruyens", t => t.LoaiTruyenID, cascadeDelete: true)
                .Index(t => t.LoaiTruyenID);
            
            CreateTable(
                "dbo.LoaiTruyens",
                c => new
                    {
                        LoaiTruyenID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.LoaiTruyenID);
            
            CreateTable(
                "dbo.BaiDangs_Tags",
                c => new
                    {
                        BaiDangID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BaiDangID, t.TagID })
                .ForeignKey("dbo.BaiDangs", t => t.BaiDangID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.BaiDangID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.BaiDangs_Tags", new[] { "TagID" });
            DropIndex("dbo.BaiDangs_Tags", new[] { "BaiDangID" });
            DropIndex("dbo.ChiTietTheLoaiTruyens", new[] { "LoaiTruyenID" });
            DropIndex("dbo.HinhAnhs", new[] { "TheLoaiHinhAnhID" });
            DropIndex("dbo.HinhAnhs", new[] { "BaiDangID" });
            DropIndex("dbo.Likes", new[] { "BaiDangID" });
            DropIndex("dbo.Comments", new[] { "BaiDangID" });
            DropIndex("dbo.BaiDangs", new[] { "ChiTietTheLoaiTruyenID" });
            DropIndex("dbo.BaiDangs", new[] { "AccountID" });
            DropIndex("dbo.Accounts", new[] { "TuocViID" });
            DropForeignKey("dbo.BaiDangs_Tags", "TagID", "dbo.Tags");
            DropForeignKey("dbo.BaiDangs_Tags", "BaiDangID", "dbo.BaiDangs");
            DropForeignKey("dbo.ChiTietTheLoaiTruyens", "LoaiTruyenID", "dbo.LoaiTruyens");
            DropForeignKey("dbo.HinhAnhs", "TheLoaiHinhAnhID", "dbo.TheLoaiHinhAnhs");
            DropForeignKey("dbo.HinhAnhs", "BaiDangID", "dbo.BaiDangs");
            DropForeignKey("dbo.Likes", "BaiDangID", "dbo.BaiDangs");
            DropForeignKey("dbo.Comments", "BaiDangID", "dbo.BaiDangs");
            DropForeignKey("dbo.BaiDangs", "ChiTietTheLoaiTruyenID", "dbo.ChiTietTheLoaiTruyens");
            DropForeignKey("dbo.BaiDangs", "AccountID", "dbo.Accounts");
            DropForeignKey("dbo.Accounts", "TuocViID", "dbo.TuocVis");
            DropTable("dbo.BaiDangs_Tags");
            DropTable("dbo.LoaiTruyens");
            DropTable("dbo.ChiTietTheLoaiTruyens");
            DropTable("dbo.TheLoaiHinhAnhs");
            DropTable("dbo.HinhAnhs");
            DropTable("dbo.Likes");
            DropTable("dbo.Comments");
            DropTable("dbo.Tags");
            DropTable("dbo.BaiDangs");
            DropTable("dbo.TuocVis");
            DropTable("dbo.Accounts");
        }
    }
}
