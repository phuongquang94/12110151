namespace MyWebFinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Third : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaiKhoan", "TenTaiKhoan", c => c.String());
            AddColumn("dbo.TaiKhoan", "TenNguoiDung", c => c.String());
            AddColumn("dbo.TaiKhoan", "TenHienThi", c => c.String());
            AddColumn("dbo.TaiKhoan", "NgaySinh", c => c.DateTime(nullable: false));
            AddColumn("dbo.TaiKhoan", "GioiTinh", c => c.String());
            AddColumn("dbo.TaiKhoan", "DiaChi", c => c.String());
            AddColumn("dbo.TaiKhoan", "Email", c => c.String());
            AddColumn("dbo.TaiKhoan", "SoDienThoai", c => c.String());
            AddColumn("dbo.TaiKhoan", "SoThich", c => c.String());
            AddColumn("dbo.TuocVi", "TenTuocVi", c => c.String());
            AddColumn("dbo.TuocVi", "DieuKienBaiDang", c => c.String());
            AddColumn("dbo.BaiDang", "TacGia", c => c.String());
            AddColumn("dbo.BaiDang", "TenBaiDang", c => c.String());
            AddColumn("dbo.BaiDang", "NgayDang", c => c.DateTime(nullable: false));
            AddColumn("dbo.Tag", "NoiDung", c => c.String());
            AddColumn("dbo.BinhLuan", "AccountID", c => c.Int(nullable: false));
            AddColumn("dbo.BinhLuan", "NoiDung", c => c.String());
            AddColumn("dbo.BinhLuan", "NgayComment", c => c.DateTime(nullable: false));
            AddColumn("dbo.Thich", "AccountID", c => c.Int(nullable: false));
            AddColumn("dbo.Thich", "NgayThich", c => c.String());
            AddColumn("dbo.HinhAnh", "TenHinhAnh", c => c.String());
            AddColumn("dbo.TheLoaiHinhAnh", "TenLoaiHinhAnh", c => c.String());
            AddColumn("dbo.ChiTietTheLoaiTruyen", "TenChiTietTheLoaiTruyen", c => c.String());
            AddColumn("dbo.LoaiTruyen", "TenLoaiTruyen", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LoaiTruyen", "TenLoaiTruyen");
            DropColumn("dbo.ChiTietTheLoaiTruyen", "TenChiTietTheLoaiTruyen");
            DropColumn("dbo.TheLoaiHinhAnh", "TenLoaiHinhAnh");
            DropColumn("dbo.HinhAnh", "TenHinhAnh");
            DropColumn("dbo.Thich", "NgayThich");
            DropColumn("dbo.Thich", "AccountID");
            DropColumn("dbo.BinhLuan", "NgayComment");
            DropColumn("dbo.BinhLuan", "NoiDung");
            DropColumn("dbo.BinhLuan", "AccountID");
            DropColumn("dbo.Tag", "NoiDung");
            DropColumn("dbo.BaiDang", "NgayDang");
            DropColumn("dbo.BaiDang", "TenBaiDang");
            DropColumn("dbo.BaiDang", "TacGia");
            DropColumn("dbo.TuocVi", "DieuKienBaiDang");
            DropColumn("dbo.TuocVi", "TenTuocVi");
            DropColumn("dbo.TaiKhoan", "SoThich");
            DropColumn("dbo.TaiKhoan", "SoDienThoai");
            DropColumn("dbo.TaiKhoan", "Email");
            DropColumn("dbo.TaiKhoan", "DiaChi");
            DropColumn("dbo.TaiKhoan", "GioiTinh");
            DropColumn("dbo.TaiKhoan", "NgaySinh");
            DropColumn("dbo.TaiKhoan", "TenHienThi");
            DropColumn("dbo.TaiKhoan", "TenNguoiDung");
            DropColumn("dbo.TaiKhoan", "TenTaiKhoan");
        }
    }
}
