namespace MyWebFinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Second : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Accounts", newName: "TaiKhoan");
            RenameTable(name: "dbo.TuocVis", newName: "TuocVi");
            RenameTable(name: "dbo.BaiDangs", newName: "BaiDang");
            RenameTable(name: "dbo.Tags", newName: "Tag");
            RenameTable(name: "dbo.Comments", newName: "BinhLuan");
            RenameTable(name: "dbo.Likes", newName: "Thich");
            RenameTable(name: "dbo.HinhAnhs", newName: "HinhAnh");
            RenameTable(name: "dbo.TheLoaiHinhAnhs", newName: "TheLoaiHinhAnh");
            RenameTable(name: "dbo.ChiTietTheLoaiTruyens", newName: "ChiTietTheLoaiTruyen");
            RenameTable(name: "dbo.LoaiTruyens", newName: "LoaiTruyen");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.LoaiTruyen", newName: "LoaiTruyens");
            RenameTable(name: "dbo.ChiTietTheLoaiTruyen", newName: "ChiTietTheLoaiTruyens");
            RenameTable(name: "dbo.TheLoaiHinhAnh", newName: "TheLoaiHinhAnhs");
            RenameTable(name: "dbo.HinhAnh", newName: "HinhAnhs");
            RenameTable(name: "dbo.Thich", newName: "Likes");
            RenameTable(name: "dbo.BinhLuan", newName: "Comments");
            RenameTable(name: "dbo.Tag", newName: "Tags");
            RenameTable(name: "dbo.BaiDang", newName: "BaiDangs");
            RenameTable(name: "dbo.TuocVi", newName: "TuocVis");
            RenameTable(name: "dbo.TaiKhoan", newName: "Accounts");
        }
    }
}
